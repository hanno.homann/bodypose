import math
import yaml
import os

import cv2
import numpy as np
import torch
import torch.nn as nn

from lw_pose_est.modules.pose import Pose
from lw_pose_est.modules.keypoints import extract_keypoints, group_keypoints

kpt_names = ['nose', 'neck',                                          # base
             'r_sho', 'r_elb', 'r_wri', 'l_sho', 'l_elb', 'l_wri',    # arms
             'r_hip', 'r_knee', 'r_ank', 'l_hip', 'l_knee', 'l_ank',  # legs
             'r_eye', 'l_eye', 'r_ear', 'l_ear'                       # head
             ]

pose_names = ['p0_ref', 'p1_up', 'p2_down', 'p3_left', 'p4_right', 'p5_left_tilt', 'p6_right_tilt']
#pose_names = ['p0_ref', 'p1_arm_r_up', 'p2_arm_r_left', 'p3_arm_r_right', 'p4_arm_l_up', 'p5_arm_l_left', 'p6_arm_l_right']

nose_id = 0
neck_id = 1
r_sho_id = 2
r_elb_id = 3
r_wri_id = 4
l_sho_id = 5
l_elb_id = 6
l_wri_id = 7
r_hip_id = 8
r_knee_id = 9
r_ank_id = 10
l_hip_id = 11
l_knee_id = 12
l_ank_id = 13
r_eye_id = 14
l_eye_id = 15
r_ear_id = 16
l_ear_id = 17

# from val.py
def normalize(img, img_mean, img_scale):
    img = np.array(img, dtype=np.float32)
    img = (img - img_mean) * img_scale
    return img

# from val.py
def pad_width(img, stride, pad_value, min_dims):
    h, w, _ = img.shape
    h = min(min_dims[0], h)
    min_dims[0] = math.ceil(min_dims[0] / float(stride)) * stride
    min_dims[1] = max(min_dims[1], w)
    min_dims[1] = math.ceil(min_dims[1] / float(stride)) * stride
    pad = []
    pad.append(int(math.floor((min_dims[0] - h) / 2.0)))
    pad.append(int(math.floor((min_dims[1] - w) / 2.0)))
    pad.append(int(min_dims[0] - h - pad[0]))
    pad.append(int(min_dims[1] - w - pad[1]))
    padded_img = cv2.copyMakeBorder(img, pad[0], pad[2], pad[1], pad[3],
                                    cv2.BORDER_CONSTANT, value=pad_value)
    return padded_img, pad

class ImageReader(object):
    def __init__(self, folder_path):
        self.file_names = self.get_png_files(folder_path)
        self.max_idx = len(self.file_names)

    def __iter__(self):
        self.idx = 0
        return self

    def __next__(self):
        if self.idx == self.max_idx:
            raise StopIteration
        img = cv2.imread(self.file_names[self.idx], cv2.IMREAD_COLOR)
        if img.size == 0:
            raise IOError('Image {} cannot be read'.format(self.file_names[self.idx]))
        self.idx = self.idx + 1
        return img
    
    @staticmethod
    def get_png_files(folder_path):
        png_files = []
        for file in os.listdir(folder_path):
            if file.endswith(".png"):
                png_files.append(os.path.join(folder_path, file))
        return png_files


class VideoReader(object):
    def __init__(self, file_name):
        self.file_name = file_name
        if type(file_name) == int:
            # stream from cam (using DSHOW instead of MSMF)
            self.cap = cv2.VideoCapture(self.file_name + cv2.CAP_DSHOW)
        else:
            # stream from file
            self.cap = cv2.VideoCapture(self.file_name)
            
        
    def __del__(self):
        # release the camera
        self.cap.release()

    def __iter__(self):
        if not self.cap.isOpened():
            raise IOError('Video {} cannot be opened'.format(self.file_name))
        return self

    def __next__(self):
        was_read, img = self.cap.read()
        if not was_read:
            raise StopIteration
        return img

def infer_fast(net, img, net_input_height_size, stride, upsample_ratio, cpu,
               pad_value=(0, 0, 0), img_mean=np.array([128, 128, 128], np.float32), img_scale=np.float32(1/256)):
    height, width, _ = img.shape
    scale = net_input_height_size / height

    scaled_img = cv2.resize(img, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_LINEAR)
    scaled_img = normalize(scaled_img, img_mean, img_scale)
    min_dims = [net_input_height_size, max(scaled_img.shape[1], net_input_height_size)]
    padded_img, pad = pad_width(scaled_img, stride, pad_value, min_dims)

    tensor_img = torch.from_numpy(padded_img).permute(2, 0, 1).unsqueeze(0).float()
    if not cpu:
        tensor_img = tensor_img.cuda()

    stages_output = net(tensor_img)

    stage2_heatmaps = stages_output[-2]
    heatmaps = np.transpose(stage2_heatmaps.squeeze().cpu().data.numpy(), (1, 2, 0))
    heatmaps = cv2.resize(heatmaps, (0, 0), fx=upsample_ratio, fy=upsample_ratio, interpolation=cv2.INTER_CUBIC)

    stage2_pafs = stages_output[-1]
    pafs = np.transpose(stage2_pafs.squeeze().cpu().data.numpy(), (1, 2, 0))
    pafs = cv2.resize(pafs, (0, 0), fx=upsample_ratio, fy=upsample_ratio, interpolation=cv2.INTER_CUBIC)

    return heatmaps, pafs, scale, pad

def infer_poses(net, img, height_size, gpu):
    stride = 8
    upsample_ratio = 4
    num_keypoints = Pose.num_kpts

    cpu = not gpu
    heatmaps, pafs, scale, pad = infer_fast(net, img, height_size, stride, upsample_ratio, cpu)

    total_keypoints_num = 0
    all_keypoints_by_type = []
    for kpt_idx in range(num_keypoints):  # 19th for bg
        total_keypoints_num += extract_keypoints(heatmaps[:, :, kpt_idx], all_keypoints_by_type, total_keypoints_num)

    pose_entries, all_keypoints = group_keypoints(all_keypoints_by_type, pafs)
    for kpt_id in range(all_keypoints.shape[0]):
        all_keypoints[kpt_id, 0] = (all_keypoints[kpt_id, 0] * stride / upsample_ratio - pad[1]) / scale
        all_keypoints[kpt_id, 1] = (all_keypoints[kpt_id, 1] * stride / upsample_ratio - pad[0]) / scale
    current_poses = []
    for n in range(len(pose_entries)):
        if len(pose_entries[n]) == 0:
            continue
        pose_keypoints = np.ones((num_keypoints, 2), dtype=np.int32) * -1
        pose_ktp_scores = np.ones((num_keypoints), dtype=np.float32) * -1
        for kpt_id in range(num_keypoints):
            if pose_entries[n][kpt_id] != -1.0:  # keypoint was found
                pose_keypoints[kpt_id, 0] = int(all_keypoints[int(pose_entries[n][kpt_id]), 0])
                pose_keypoints[kpt_id, 1] = int(all_keypoints[int(pose_entries[n][kpt_id]), 1])
                pose_ktp_scores[kpt_id] = all_keypoints[int(pose_entries[n][kpt_id]), 2]
        pose_confidence = pose_entries[n][18]
        pose = Pose(pose_keypoints, pose_confidence)
        current_poses.append(pose)

    # sort poses by confidence
    current_poses = sorted(current_poses, key=lambda pose: pose.confidence, reverse=True)
    
    return current_poses

def save_pose(pose, label, img, filename_base):
    print(f"Saving pose: {filename_base}")
    cv2.imwrite(filename_base + '.png', img)

    pose_dict = {}
    pose_dict['keypoints'] = pose.keypoints.tolist()
    pose_dict['label'] = label
    filename_yaml = filename_base + '.yaml'
    with open(filename_yaml, 'w') as f:
        yaml.dump(pose_dict, f)

def load_poses(folder, poses = []):    
    items = os.listdir(folder)
    for filename in items:
        if filename[-5:] != '.yaml':
            continue

        with open(os.path.join(folder,filename)) as f:
            pose_dict = yaml.safe_load(f)

        pose_dict['keypoints'] = np.array(pose_dict['keypoints'])
        pose_dict['filename'] = filename

        #label = pose_dict['label']
        #if label == 'p0_ref':
        #    continue
        poses.append(pose_dict)

class NormalizedPose(Pose):
    def __init__(self, keypoints):
        Pose.__init__(self, keypoints, confidence=0)
        
        if not NormalizedPose.isvalid_keypoints(keypoints):
            raise AssertionError("Eyes, nose, and neck must be detected!")
            self.scale = 0
            self.diffpoints = np.ones((17,2)) * -1
            return
        
        # nose-neck worked better for scale
        self.scale = np.linalg.norm(keypoints[nose_id,:] - keypoints[neck_id,:])
        #self.scale = np.linalg.norm(keypoints[l_eye_id,:] - keypoints[r_eye_id,:])
    
        if True: # neighbors
            self.diffpoints = np.array((
                self.to_diff(keypoints[nose_id,:],  keypoints[neck_id,:]),
                self.to_diff(keypoints[r_sho_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[r_elb_id,:], keypoints[r_sho_id,:]),
                self.to_diff(keypoints[r_wri_id,:], keypoints[r_elb_id,:]),
                self.to_diff(keypoints[l_sho_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_elb_id,:], keypoints[l_sho_id,:]),
                self.to_diff(keypoints[l_wri_id,:], keypoints[l_elb_id,:]),
                self.to_diff(keypoints[r_hip_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[r_knee_id,:], keypoints[r_hip_id,:]),
                self.to_diff(keypoints[r_ank_id,:], keypoints[r_knee_id,:]),
                self.to_diff(keypoints[l_hip_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_knee_id,:], keypoints[l_hip_id,:]),
                self.to_diff(keypoints[l_ank_id,:], keypoints[l_knee_id,:]),
    
                self.to_diff(keypoints[r_eye_id,:], keypoints[nose_id,:]),
                self.to_diff(keypoints[r_ear_id,:], keypoints[r_eye_id,:]),
                self.to_diff(keypoints[l_eye_id,:], keypoints[nose_id,:]),
                self.to_diff(keypoints[l_ear_id,:], keypoints[l_eye_id,:]),
            ))
        else: # rel to neck -> bad validation results
            self.diffpoints = np.array((
                self.to_diff(keypoints[nose_id,:],  keypoints[neck_id,:]),
                self.to_diff(keypoints[r_sho_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[r_elb_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[r_wri_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_sho_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_elb_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_wri_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[r_hip_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[r_knee_id,:],keypoints[neck_id,:]),
                self.to_diff(keypoints[r_ank_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_hip_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_knee_id,:],keypoints[neck_id,:]),
                self.to_diff(keypoints[l_ank_id,:], keypoints[neck_id,:]),
    
                self.to_diff(keypoints[r_eye_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[r_ear_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_eye_id,:], keypoints[neck_id,:]),
                self.to_diff(keypoints[l_ear_id,:], keypoints[neck_id,:]),
            ))
        
    def to_diff(self, kpt1, kpt2):
        if NormalizedPose.isvalid(kpt1) and NormalizedPose.isvalid(kpt2):
            return (kpt1 - kpt2) / self.scale            
        else:
            return np.array((-1, -1))
        
    def length(self):
        return self.diffpoints.shape[0]
        
    @staticmethod
    def isvalid(kpt):
        if kpt[0] == -1 or kpt[1] == -1:
            return False
        else:
            return True

    @staticmethod
    def isvalid_keypoints(keypoints):
        if NormalizedPose.isvalid(keypoints[nose_id,:]) and \
           NormalizedPose.isvalid(keypoints[neck_id,:]) and \
           NormalizedPose.isvalid(keypoints[l_eye_id,:]) and \
           NormalizedPose.isvalid(keypoints[r_eye_id,:]) :
            return True
        else:
            return False

class PositionModel(nn.Module):
    def __init__(self):
        super(PositionModel, self).__init__()

        num_poses = len(pose_names)
        num_kpts = 17 #18

        self.weights = torch.nn.Parameter(torch.rand((num_poses, num_kpts*2)))
        self.bias    = torch.nn.Parameter(torch.rand((num_poses)))
        
        #self.classifier = nn.Sequential(
        #    nn.Linear(in_features=num_kpts*2, out_features=num_poses),
        #)

    def forward(self, keypoints):
        x = torch.flatten(keypoints)
        mask = (x != -1)
        x = x*mask
        #z_out = self.classifier(x)
        z_out = self.weights @ x + self.bias
        probabilities = nn.functional.softmax(z_out, dim=0)
        return probabilities

