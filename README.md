# Installation
Set-up python:
```
py -m pip install -r requirements.txt
```

Install git sub-repositories:
```
git submodule update
```

Download pre-trained model and save to 'models' directory:
https://download.01.org/opencv/openvino_training_extensions/models/human_pose_estimation/checkpoint_iter_370000.pth

Run the demo:
```
py lightweight-human-pose-estimation.pytorch/demo.py --checkpoint-path models/checkpoint_iter_370000.pth --video 0 --cpu
```