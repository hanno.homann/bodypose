import argparse
import os
import sys

import cv2
#import numpy as np
import torch
import time
import win32api # much faster than pyautogui

sys.path.append(os.path.join(os.path.dirname(__file__), 'lw_pose_est'))

from lw_pose_est.models.with_mobilenet import PoseEstimationWithMobileNet
from lw_pose_est.modules.load_state import load_state
from lw_pose_est.modules.pose import Pose, track_poses

from common import ImageReader, VideoReader, infer_poses, pose_names, NormalizedPose, PositionModel

control_state = 'normal'
control_clicks_enabled = False
control_mouse_step = 5

def control_mouse(predicted_name, p_max):
    global control_state

    if predicted_name == 'p1_up' and p_max >= 0.5:
        control_mouse_x, control_mouse_y = win32api.GetCursorPos()
        win32api.SetCursorPos((control_mouse_x, control_mouse_y-control_mouse_step))
    elif predicted_name == 'p2_down' and p_max >= 0.5:
        control_mouse_x, control_mouse_y = win32api.GetCursorPos()
        win32api.SetCursorPos((control_mouse_x, control_mouse_y+control_mouse_step))
    elif predicted_name == 'p3_left' and p_max >= 0.5:
        control_mouse_x, control_mouse_y = win32api.GetCursorPos()
        win32api.SetCursorPos((control_mouse_x-control_mouse_step, control_mouse_y))
    elif predicted_name == 'p4_right' and p_max >= 0.5:
        control_mouse_x, control_mouse_y = win32api.GetCursorPos()
        win32api.SetCursorPos((control_mouse_x+control_mouse_step, control_mouse_y))
    elif predicted_name == 'p5_left_tilt' and p_max >= 0.5:
        if control_state != 'escape' and control_clicks_enabled:
            win32api.MessageBox(0, 'ESCAPE', 'title')
            control_state = 'escape'
        pass
    elif predicted_name == 'p6_right_tilt' and p_max >= 0.5:
        if control_state != 'enter' and control_clicks_enabled:
            win32api.MessageBox(0, 'ENTER', 'title')
            control_state = 'enter'
        pass
               
def run_demo(net, position_model, image_provider, height_size, gpu, track, smooth, delay=1):
    net = net.eval()
    if gpu:
        net = net.cuda()

    previous_poses = []

    current_time = time.time()
    for img in image_provider:
        orig_img = img.copy()


        prev_time = current_time
        current_poses = infer_poses(net, img, height_size, gpu)
        current_time = time.time()
        
        if track:
            track_poses(previous_poses, current_poses, smooth=smooth)
            previous_poses = current_poses
        for pose in current_poses:
            Pose.color = [232, 162, 0]
            pose.draw(img)
        img = cv2.addWeighted(orig_img, 0.6, img, 0.4, 0)
        img = cv2.flip(img, 1) # mirror horizontally
        
        if len(current_poses) > 0:
            current_pose = current_poses[0].keypoints
            
            if NormalizedPose.isvalid_keypoints(current_pose):
                cpose = NormalizedPose(current_pose)
                keypoints = torch.tensor(cpose.diffpoints, dtype=torch.float32)[None, :, :]
                
                y_probabilities = position_model(keypoints)
                
                p_max, predicted_label = torch.max(y_probabilities, 0)
                predicted_name = pose_names[predicted_label]
                cv2.putText(img, f'test: {predicted_name}, p={p_max*100:.1f}', (20, 20), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255))

                control_mouse(predicted_name, p_max)
         
        cv2.imshow('Infer poses', img)
        key = cv2.waitKey(delay)
        if key == 27:  # esc
            break
        
        delta_time = current_time - prev_time
        frames_per_second = 1.0 / delta_time
        print(f"--- {frames_per_second:.1f} fps ---")
        
    # close the window and free memory
    cv2.destroyAllWindows()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''Lightweight human pose estimation python demo.
                       This is just for quick results preview.
                       Please, consider c++ demo for the best performance.''')
    parser.add_argument('--checkpoint-path', type=str, default='models/checkpoint_iter_370000.pth', help='path to the checkpoint')
    parser.add_argument('--height-size', type=int, default=256, help='network input layer height size')
    parser.add_argument('--video', type=str, default=0, help='path to video file or camera id')
    parser.add_argument('--images', nargs='+', default='', help='path to input image(s)')
    parser.add_argument('--gpu', default=True, action='store_true', help='run network inference on gpu')
    parser.add_argument('--track', type=int, default=1, help='track pose id in video')
    parser.add_argument('--smooth', type=int, default=1, help='smooth pose keypoints')
    args = parser.parse_args()

    if args.video == '' and args.images == '':
        raise ValueError('Either --video or --image has to be provided')

    net = PoseEstimationWithMobileNet()
    checkpoint = torch.load(args.checkpoint_path, map_location='cpu')
    load_state(net, checkpoint)

    #args.images = r'C:\workspace\bodytalk\train_poses\head2_hh'
    if args.images == '':
        frame_provider = VideoReader(args.video)
        delay = 1
    else:
        frame_provider = ImageReader(args.images)
        args.track = 0
        delay = 0

    position_model = PositionModel()
    position_model.load_state_dict(torch.load('models/position_model_noref.pth'))
    position_model.eval()
    
    run_demo(net, position_model, frame_provider, args.height_size, args.gpu, args.track, args.smooth, delay)
