import torch
import matplotlib.pyplot as plt
import sys
import os
import numpy as np
import seaborn as sn
import pandas as pd
import time

sys.path.append(os.path.join(os.path.dirname(__file__), 'lw_pose_est'))

from common import load_poses, pose_names, NormalizedPose, PositionModel

def class_encoding(label):
    assert(label in pose_names)

    for i, pose_name in enumerate(pose_names):
        if label == pose_name:
            return i
    assert(False)

def cross_entropy_loss(y_probabilities, y_true):
    loss_sum = torch.tensor(0.0)
    loss_sum += (-1)*torch.log(y_probabilities[y_true])
    return loss_sum

def get_accuracy(poses, model):
    with torch.no_grad():
        model.eval()
    
        n = 0
        correct_pred = 0
        for pose in poses:
            label_num = class_encoding(pose['label'])
            keypoints = pose['keypoints']

            npose = NormalizedPose(keypoints)
            keypoints = torch.tensor(npose.diffpoints, dtype=torch.float32)[None, :, :]
            
            y_probabilities = model(keypoints)
    
            _, predicted_label = torch.max(y_probabilities, 0)

            n += 1
            correct_pred += (predicted_label == label_num).sum()

    return correct_pred.float() / n

def get_confusion_matrix(poses, model):
    confusion_matrix = np.zeros((len(pose_names),len(pose_names)), dtype=np.uint32)
    with torch.no_grad():
        model.eval()
    
        for pose in poses:
            label_num = class_encoding(pose['label'])
            keypoints = pose['keypoints']

            npose = NormalizedPose(keypoints)
            keypoints = torch.tensor(npose.diffpoints, dtype=torch.float32)[None, :, :]
            
            y_probabilities = model(keypoints)
    
            _, predicted_label = torch.max(y_probabilities, 0)

            confusion_matrix[predicted_label, label_num] += 1

    return confusion_matrix

def train(train_poses, model, optimizer, epoch):
    model.train()
    loss_sum = 0

    for train_pose in train_poses:
        optimizer.zero_grad()

        train_label_num = class_encoding(train_pose['label'])
        keypoints = train_pose['keypoints']
        
        # drop-out:
        drop_out = False
        if drop_out:
            num_keypoints = keypoints.shape[0]
            drop_kpt_idx = epoch % (num_keypoints-2) + 2 # never drop nose(0) and neck(1)!
            keypoints_dropout = keypoints.copy()
            keypoints_dropout[drop_kpt_idx, :] = -1
            keypoints = keypoints_dropout
        
        npose = NormalizedPose(keypoints)
        keypoints = torch.tensor(npose.diffpoints, dtype=torch.float32)[None, :, :]
        
        y_probabilities = model(keypoints)
        
        loss = cross_entropy_loss(y_probabilities, train_label_num)
        loss_sum += loss

        loss.backward()
        optimizer.step()
        
    epoch_loss = loss_sum / len(train_poses)
    return epoch_loss.detach().numpy()    

def training_loop(model, optimizer, train_poses, valid_poses, epochs):
    train_losses = []
    train_accs = []
    valid_accs = []
    
    for epoch in range(0, epochs):
        train_loss = train(train_poses, model, optimizer, epoch)
        train_acc = get_accuracy(train_poses, model)
        valid_acc = get_accuracy(valid_poses, model)

        train_losses.append(train_loss)
        train_accs.append(train_acc)
        valid_accs.append(valid_acc)
           
        print(f'Epoch: {epoch}\t'
             f'Train loss: {train_loss:.4f}\t'
             f'Train/Val accuracy: {100 * train_acc:.2f} / {100 * valid_acc:.2f}\t')
        

    plt.rcParams.update({'font.size': 12})
    plt.plot(train_losses)
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.show()

    plt.plot(train_accs, label='training')
    plt.plot(valid_accs, label='validation')
    plt.legend()
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.show()

    
    confusion_matrix = get_confusion_matrix(valid_poses, model)
    pose_names_cm = ['ref', 'up', 'down', 'left_turn', 'right_turn', 'left_tilt', 'right_tilt']

    #fig = plt.figure()
    #fig.patch.set_facecolor('xkcd:mint green')
    plt.rcParams.update({'font.size': 20})
    df_cm = pd.DataFrame(confusion_matrix, index = pose_names_cm, columns = pose_names_cm)
    #plt.figure(figsize = (10,7))
    sn.heatmap(df_cm, annot=True)
    plt.xlabel('ground-truth label') #, fontweight='bold')
    plt.ylabel('predicted label')    #, fontweight='bold')
    plt.savefig('fig4_confusion_matrix.svg', dpi=300)
    plt.show()
    
    # width="500pt" height="400pt" viewBox="-30 20 340 368"


if __name__ == '__main__':
    torch.manual_seed(1)
    train_folder = 'train_poses\\head_hh3'
    train_poses = []
    load_poses(train_folder, train_poses)
    
    # cross-patient:
    valid_folders = ('train_poses\\head_cr1', 'train_poses\\head_fs1',
                     'train_poses\\head_ts1', 'train_poses\\head_sh2',
                     'train_poses\\head_jh1', 'train_poses\\head_jw1')
    
    ## inter-patient:
    #valid_folders = ('train_poses\\head_hh4',)# 'train_poses\\head_hh5',
    #                 #'train_poses\\head_hh8', 'train_poses\\head_hh9')
    valid_poses = []
    for valid_folder in valid_folders:
        load_poses(valid_folder, valid_poses)
    
    model = PositionModel()
    LEARNING_RATE = 0.1
    N_EPOCHS = 200
    weight_decay=1e-4

    optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE, weight_decay=weight_decay)

    start_time = time.time()
    training_loop(model, optimizer, train_poses, valid_poses, N_EPOCHS)
    delta_time = time.time() - start_time
    print(f"--- Training time: {delta_time:.1f} sec ---")


    torch.save(model.state_dict(), 'models/position_model_test.pth')
    
    #print("Model's state_dict:")
    #for param_tensor in model.state_dict():
    #    print(param_tensor, "\t", model.state_dict()[param_tensor])
    